// Copyright (c) 2021 David Ewelt <uranoxyd@gmail.com>, released under MIT License. See LICENSE file.

package mergingfs

import (
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

type MergingFS struct {
	fileSystems []fs.FS
}

func (mfs *MergingFS) Open(name string) (fs.File, error) {
	name = strings.ReplaceAll(name, "\\", "/")
	for _, fsys := range mfs.fileSystems {
		n := name
		file, err := fsys.Open(n)
		if err != nil {
			if os.IsNotExist(err) {
				continue
			} else {
				return nil, err
			}
		} else {
			return file, nil
		}
	}

	return nil, os.ErrNotExist
}
func (mfs *MergingFS) ReadDir(name string) ([]fs.DirEntry, error) {
	name = strings.ReplaceAll(name, "\\", "/")
	var entryMap map[string]os.DirEntry = make(map[string]fs.DirEntry)

	for _, fsys := range mfs.fileSystems {
		if entries, err := fs.ReadDir(fsys, name); err == nil {
			for _, entry := range entries {
				entryMap[entry.Name()] = entry
			}
		} else if !os.IsNotExist(err) {
			return nil, err
		}
	}

	var result []fs.DirEntry
	for _, entry := range entryMap {
		result = append(result, entry)
	}

	return result, nil
}
func (mfs *MergingFS) ReadFile(name string) ([]byte, error) {
	name = strings.ReplaceAll(name, "\\", "/")
	for _, fsys := range mfs.fileSystems {
		contents, err := fs.ReadFile(fsys, name)
		if err != nil {
			if os.IsNotExist(err) {
				continue
			} else {
				return nil, err
			}
		} else {
			return contents, nil
		}
	}

	return nil, os.ErrNotExist
}

func WalkFS(fsys fs.FS, path string, fn func(fsys fs.FS, path string)) {
	path = strings.ReplaceAll(path, "\\", "/")
	entries, err := fs.ReadDir(fsys, path)
	if err != nil {
		return
	}
	for _, entry := range entries {
		entryPath := filepath.Join(path, entry.Name())
		if entry.IsDir() {
			WalkFS(fsys, entryPath, fn)
			continue
		}
		fn(fsys, entryPath)
	}
}

func NewMergingFS(fileSystems ...fs.FS) *MergingFS {
	return &MergingFS{fileSystems: fileSystems}
}
