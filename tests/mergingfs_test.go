package tests

import (
	"embed"
	"io/fs"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"gitlab.com/uranoxyd/mergingfs"
)

//go:embed primary
var embedFS embed.FS

func createFS() *mergingfs.MergingFS {
	efs, err := fs.Sub(embedFS, "primary")
	if err != nil {
		panic(err)
	}
	nfs := os.DirFS("secondary")

	return mergingfs.NewMergingFS(nfs, efs)
}

func openAndCompare(t *testing.T, fsys *mergingfs.MergingFS, name string, should string) {
	file, err := fsys.Open(name)
	if err != nil {
		t.Fatalf("Open(%s) raised an error: %v", name, err)
		return
	}
	content, err := ioutil.ReadAll(file)
	if err != nil {
		t.Fatalf("reading '%s' raised an error: %v", name, err)
		return
	}
	if string(content) != should {
		t.Fatalf("content of '%s' should be '%s' but was '%s'", name, should, string(content))
		return
	}
}

func readFileAndCompare(t *testing.T, fsys *mergingfs.MergingFS, name string, should string) {
	content, err := fsys.ReadFile(name)
	if err != nil {
		t.Fatalf("ReadFile(%s) raised an error: %v", name, err)
		return
	}
	if string(content) != should {
		t.Fatalf("content of '%s' should be '%s' but was '%s'", name, should, string(content))
		return
	}
}

func readDirAndCompare(t *testing.T, fsys *mergingfs.MergingFS, name string, expectedEntries ...string) {
	returnedEntries, err := fsys.ReadDir(name)
	if err != nil {
		t.Fatalf("ReadDir(%s) raised an error: %v", name, err)
		return
	}

	var missingEntries []string
	var unexpectedEntries []string

	for _, expectedEntry := range expectedEntries {
		found := false
		for _, returnedEntry := range returnedEntries {
			if expectedEntry == returnedEntry.Name() {
				found = true
				break
			}
		}

		if !found {
			missingEntries = append(missingEntries, expectedEntry)
		}
	}

	for _, returnedEntry := range returnedEntries {
		found := false
		for _, expectedEntry := range expectedEntries {
			if expectedEntry == returnedEntry.Name() {
				found = true
				break
			}
		}

		if !found {
			unexpectedEntries = append(missingEntries, returnedEntry.Name())
		}
	}

	if len(unexpectedEntries) > 0 {
		t.Fatalf("ReadDir(%s) returned more entries then expected, unexpected entries are: %s", name, strings.Join(unexpectedEntries, ", "))
		return
	}

	if len(missingEntries) > 0 {
		t.Fatalf("ReadDir(%s) did not returned all expected paths, missing paths are: %s", name, strings.Join(missingEntries, ", "))
	}
}

func TestOpen(t *testing.T) {
	fsys := createFS()

	openAndCompare(t, fsys, "foo.txt", "primary.foo (overriten by secondary fs)")
	openAndCompare(t, fsys, "foo/bar.txt", "primary.foo.bar")
	openAndCompare(t, fsys, "bar/baz.txt", "secondary.bar.baz")
}

func TestReadFile(t *testing.T) {
	fsys := createFS()

	readFileAndCompare(t, fsys, "foo.txt", "primary.foo (overriten by secondary fs)")
	readFileAndCompare(t, fsys, "foo/bar.txt", "primary.foo.bar")
	readFileAndCompare(t, fsys, "bar/baz.txt", "secondary.bar.baz")
}

func TestReadDir(t *testing.T) {
	fsys := createFS()

	readDirAndCompare(t, fsys, ".", "foo", "foo.txt", "bar")
	readDirAndCompare(t, fsys, "foo", "bar.txt")
	readDirAndCompare(t, fsys, "bar", "baz.txt")
}
