package main

import (
	"embed"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"

	"gitlab.com/uranoxyd/mergingfs"
)

//go:embed primary
var embedPrimary embed.FS

func main() {
	embedFS, err := fs.Sub(embedPrimary, "primary")
	if err != nil {
		panic(err)
	}
	nativeFS := os.DirFS("secondary")

	mergingFS := mergingfs.NewMergingFS(nativeFS, embedFS)

	mergingfs.WalkFS(mergingFS, ".", func(fsys fs.FS, path string) {
		fmt.Println(" -", path)
	})

	http.Handle("/", http.FileServer(http.FS(mergingFS)))
	log.Fatal(http.ListenAndServe("127.0.0.1:8000", nil))
}
