# MergingFS

I wrote this module because I had a webfrontend for an application whose resources (js, images, templates) should be embedded in the executable, nevertheless it should be possible to overwrite the embedded resources.

This is a readonly filesystem implementation, to which several other ``fs.FS`` derived filesystem implementations can be fed. For example, if ``Open`` is called into this file system, it will run through all its file systems until it finds the file. The same is true for ``ReadDir`` and ``ReadFile``.

The best way to imagine the behavior is as if you were copying several folders together, overwriting existing files.


## Installation

```
go get gitlab.com/uranoxyd/mergingfs
```

## Usage Example

```go
package main

import (
	"embed"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"

	"gitlab.com/uranoxyd/mergingfs"
)

//go:embed primary
var embedPrimary embed.FS

func main() {
	embedFS, err := fs.Sub(embedPrimary, "primary")
	if err != nil {
		panic(err)
	}
	nativeFS := os.DirFS("secondary")

	mergingFS := mergingfs.NewMergingFS(nativeFS, embedFS)

	mergingfs.WalkFS(mergingFS, ".", func(fsys fs.FS, path string) {
		fmt.Println(" -", path)
	})

	http.Handle("/", http.FileServer(http.FS(mergingFS)))
	log.Fatal(http.ListenAndServe("127.0.0.1:8000", nil))
}
```

## License
MIT